let previous = document.querySelector('#pre');
let play = document.querySelector('#play');
let next = document.querySelector('#next');
let title = document.querySelector('#title');
let recent_volume= document.querySelector('#volume');
let volume_show = document.querySelector('#volume_show');
let slider = document.querySelector('#duration_slider');
let show_duration = document.querySelector('#show_duration');
let track_image = document.querySelector('#track_image');
let auto_play = document.querySelector('#auto');
let present = document.querySelector('#present');
let total = document.querySelector('#total');
let artist = document.querySelector('#artist');



let timer;
let autoplay = 0;

let index_no = 0;
let Playing_song = false;

//create a audio Element
let track = document.createElement('audio');


//All songs list
let All_song = [
	{
		name: "Espagnola",
		path: "/asset/audios/samouth/Espagnola.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "Ye Ye ឆ្នាំ73",
		path: "/asset/audios/samouth/Ye Ye ឆ្នាំ73.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កណ្តាលដួងចិត្ត",
		path: "/asset/audios/samouth/កណ្តាលដួងចិត្ត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កណ្តាលថ្នាលជីវិត",
		path: "/asset/audios/samouth/កណ្តាលថ្នាលជីវិត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កណ្តាលរាត្រី",
		path: "/asset/audios/samouth/កណ្តាលរាត្រី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កណ្តូបបុកស្រូវ",
		path: "/asset/audios/samouth/កណ្តូបបុកស្រូវ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កន្សែងស្រីប៉ាក់",
		path: "/asset/audios/samouth/កន្សែងស្រីប៉ាក់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កប្បាសលាស់ខ្ចី",
		path: "/asset/audios/samouth/កប្បាសលាស់ខ្ចី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កម្មស្នេហ៍ស្រីអាំ",
		path: "/asset/audios/samouth/កម្មស្នេហ៍ស្រីអាំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កម្រងផួងថ្កុល",
		path: "/asset/audios/samouth/កម្រងផួងថ្កុល.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កាលណាអូនច្រៀង",
		path: "/asset/audios/samouth/កាលណាអូនច្រៀង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កាលអូននាងរាំ",
		path: "/asset/audios/samouth/កាលអូននាងរាំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កុលាបបាត់ដំបង",
		path: "/asset/audios/samouth/កុលាបបាត់ដំបង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កុលាបប៉ៃលិន",
		path: "/asset/audios/samouth/កុលាបប៉ៃលិន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កុំចងចិត្តខ្ញុំ",
		path: "/asset/audios/samouth/កុំចងចិត្តខ្ញុំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កុំឆ្លើយថាលា",
		path: "/asset/audios/samouth/កុំឆ្លើយថាលា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កុំលួចសើច",
		path: "/asset/audios/samouth/កុំលួចសើច.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កុំស្មានបងភ្លេច",
		path: "/asset/audios/samouth/កុំស្មានបងភ្លេច.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កូនកាត់3សាសន៍",
		path: "/asset/audios/samouth/កូនកាត់3សាសន៍.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កូលាបខ្មែរអាកាសចរ",
		path: "/asset/audios/samouth/កូលាបខ្មែរអាកាសចរ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កូលាបមួយទង",
		path: "/asset/audios/samouth/កូលាបមួយទង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កូលាបឥណ្ឌូនេស៊ី",
		path: "/asset/audios/samouth/កូលាបឥណ្ឌូនេស៊ី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កោះកុងខ្ញុំឣើយ",
		path: "/asset/audios/samouth/កោះកុងខ្ញុំឣើយ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កោះប្រាក់អន្ទាក់ស្នេហ៍",
		path: "/asset/audios/samouth/កោះប្រាក់អន្ទាក់ស្នេហ៍.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កំពង់ធំជុំរំចិត្ត",
		path: "/asset/audios/samouth/កំពង់ធំជុំរំចិត្ត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កំពតកំពូលដួងចិត្ត",
		path: "/asset/audios/samouth/កំពតកំពូលដួងចិត្ត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កំភេមតែងខ្លួន",
		path: "/asset/audios/samouth/កំភេមតែងខ្លួន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កំលោះញ៉េមញ៉ុមក្រមុំញ៉េវញ៉ាវ",
		path: "/asset/audios/samouth/កំលោះញ៉េមញ៉ុមក្រមុំញ៉េវញ៉ាវ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "កំលោះបីនាក់",
		path: "/asset/audios/samouth/កំលោះបីនាក់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ក្ងោកក្រមុំ",
		path: "/asset/audios/samouth/ក្ងោកក្រមុំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ក្បូនកាកី",
		path: "/asset/audios/samouth/ក្បូនកាកី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ក្របីហិតញី",
		path: "/asset/audios/samouth/ក្របីហិតញី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ក្រពុំចាកទង (បុប្ផាតែមួយ)",
		path: "/asset/audios/samouth/ក្រពុំចាកទង (បុប្ផាតែមួយ).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ក្រពើក្នុងទឹក",
		path: "/asset/audios/samouth/ក្រពើក្នុងទឹក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ក្រមុំអារញ្ញ",
		path: "/asset/audios/samouth/ក្រមុំអារញ្ញ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ក្រឡាហោមគង់",
		path: "/asset/audios/samouth/ក្រឡាហោមគង់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ក្រអូបក្លិនផ្កា",
		path: "/asset/audios/samouth/ក្រអូបក្លិនផ្កា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ក្រអូបតែក្លិន",
		path: "/asset/audios/samouth/ក្រអូបតែក្លិន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ក្លិនខ្លួននួនស្រី",
		path: "/asset/audios/samouth/ក្លិនខ្លួននួនស្រី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ក្លិនផាហ៊ុម",
		path: "/asset/audios/samouth/ក្លិនផាហ៊ុម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ក្លែបក្លិនចម្ប៉ា",
		path: "/asset/audios/samouth/ក្លែបក្លិនចម្ប៉ា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ខឹងព្រោះស្រឡាញ់",
		path: "/asset/audios/samouth/ខឹងព្រោះស្រឡាញ់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ខ្ញុំឈ្មោះជ័យជេត",
		path: "/asset/audios/samouth/ខ្ញុំឈ្មោះជ័យជេត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ខ្ញុំស្រលាញ់ស្រីតូច",
		path: "/asset/audios/samouth/ខ្ញុំស្រលាញ់ស្រីតូច.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ខ្នងភ្នំអនុស្សាវរីយ៌",
		path: "/asset/audios/samouth/ខ្នងភ្នំអនុស្សាវរីយ៌.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ខ្យងក្តក់",
		path: "/asset/audios/samouth/ខ្យងក្តក់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ខ្យល់ទន្លេ",
		path: "/asset/audios/samouth/ខ្យល់ទន្លេ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ខ្លាចប្រពន្ធដូចខ្លា",
		path: "/asset/audios/samouth/ខ្លាចប្រពន្ធដូចខ្លា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ខ្លាចអូនបានគេ",
		path: "/asset/audios/samouth/ខ្លាចអូនបានគេ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ខ្លែងដាច់ខ្សែ (ស្អែកបងលា)",
		path: "/asset/audios/samouth/ខ្លែងដាច់ខ្សែ (ស្អែកបងលា).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ខ្សែជីវិត",
		path: "/asset/audios/samouth/ខ្សែជីវិត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "គង់មានថ្ងៃស្រីដឹងខ្លួន",
		path: "/asset/audios/samouth/គង់មានថ្ងៃស្រីដឹងខ្លួន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "គីង្គក់ក្រត",
		path: "/asset/audios/samouth/គីង្គក់ក្រត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "គូរព្រេងនិស្ស័យ",
		path: "/asset/audios/samouth/គូរព្រេងនិស្ស័យ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "គូសេ្នហ៍លើនាវា",
		path: "/asset/audios/samouth/គូសេ្នហ៍លើនាវា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "គេងកើយភ្លៅ",
		path: "/asset/audios/samouth/គេងកើយភ្លៅ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "គំនុំម្ភៃឆ្នាំ",
		path: "/asset/audios/samouth/គំនុំម្ភៃឆ្នាំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ឃើញតែមួយ",
		path: "/asset/audios/samouth/ឃើញតែមួយ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ឃ្មុំផ្ញើរសំបុក",
		path: "/asset/audios/samouth/ឃ្មុំផ្ញើរសំបុក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ឃ្លាតស្ទឹងសង្កែ",
		path: "/asset/audios/samouth/ឃ្លាតស្ទឹងសង្កែ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ឃ្លាត១ថ្ងៃនិស្ស័យ១ឆ្នាំ",
		path: "/asset/audios/samouth/ឃ្លាត១ថ្ងៃនិស្ស័យ១ឆ្នាំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចង់កើតជាទឹក",
		path: "/asset/audios/samouth/ចង់កើតជាទឹក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចង់បានកន្សែងក្រហម",
		path: "/asset/audios/samouth/ចង់បានកន្សែងក្រហម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចង្រឹតស្រែកច្រៀង",
		path: "/asset/audios/samouth/ចង្រឹតស្រែកច្រៀង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចន្ទចាកមេឃ",
		path: "/asset/audios/samouth/ចន្ទចាកមេឃ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចម្ប៉ាបាត់ដំបង",
		path: "/asset/audios/samouth/ចម្ប៉ាបាត់ដំបង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចម្ប៉ាផ្សាលើ",
		path: "/asset/audios/samouth/ចម្ប៉ាផ្សាលើ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចម្រៀងសម្រាប់អូន",
		path: "/asset/audios/samouth/ចម្រៀងសម្រាប់អូន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចម្រៀងស្នេហា",
		path: "/asset/audios/samouth/ចម្រៀងស្នេហា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចម្រៀងស្នេហ៍ (ភ័ក្ត្រាស្រទន់)",
		path: "/asset/audios/samouth/ចម្រៀងស្នេហ៍ (ភ័ក្ត្រាស្រទន់).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចម្រៀងស្នេហ៍ទុំទាវ",
		path: "/asset/audios/samouth/ចម្រៀងស្នេហ៍ទុំទាវ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចម្រៀងឥតព្រាងទុក",
		path: "/asset/audios/samouth/ចម្រៀងឥតព្រាងទុក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចម្រៀង១បទតែងមិនចប់",
		path: "/asset/audios/samouth/ចម្រៀង១បទតែងមិនចប់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចម្លើយគឺទឹកភ្នែក",
		path: "/asset/audios/samouth/ចម្លើយគឺទឹកភ្នែក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចាស់ហើយលោកតា",
		path: "/asset/audios/samouth/ចាស់ហើយលោកតា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចាំច័ន្ទបំភ្លឺ",
		path: "/asset/audios/samouth/ចាំច័ន្ទបំភ្លឺ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចាំញញឺមជាថ្មី",
		path: "/asset/audios/samouth/ចាំញញឺមជាថ្មី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចាំពេញមួយរាត្រី",
		path: "/asset/audios/samouth/ចាំពេញមួយរាត្រី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចាំរាល់ថ្ងៃលិច",
		path: "/asset/audios/samouth/ចាំរាល់ថ្ងៃលិច.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចាំអូនរាល់រាត្រី",
		path: "/asset/audios/samouth/ចាំអូនរាល់រាត្រី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចិញ្ចៀនត្បូងប្រាំ",
		path: "/asset/audios/samouth/ចិញ្ចៀនត្បូងប្រាំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចិត្តខ្លាចចិត្ត",
		path: "/asset/audios/samouth/ចិត្តខ្លាចចិត្ត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចិត្តជាអ្នកបួស",
		path: "/asset/audios/samouth/ចិត្តជាអ្នកបួស.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចិត្តនៅតែគិតប្រាថ្នា",
		path: "/asset/audios/samouth/ចិត្តនៅតែគិតប្រាថ្នា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចិត្តឥតល្អៀង",
		path: "/asset/audios/samouth/ចិត្តឥតល្អៀង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចុកឈាមនៅប៉ៃលិន",
		path: "/asset/audios/samouth/ចុកឈាមនៅប៉ៃលិន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចោរចិត្តជា",
		path: "/asset/audios/samouth/ចោរចិត្តជា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចោរលួចចិត្ត",
		path: "/asset/audios/samouth/ចោរលួចចិត្ត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចៅដក",
		path: "/asset/audios/samouth/ចៅដក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចៅព្រាហ្ម",
		path: "/asset/audios/samouth/ចៅព្រាហ្ម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចំណោទស្នេហា",
		path: "/asset/audios/samouth/ចំណោទស្នេហា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចំប៉ីសួគា៌",
		path: "/asset/audios/samouth/ចំប៉ីសួគា៌.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចំប៉ីសៀមរាប",
		path: "/asset/audios/samouth/ចំប៉ីសៀមរាប.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ចំអកចំអន់",
		path: "/asset/audios/samouth/ចំអកចំអន់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ច័ន្ទរះលើរថយន្តក្រុង",
		path: "/asset/audios/samouth/ច័ន្ទរះលើរថយន្តក្រុង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ជញ្ជីងស្នេហា",
		path: "/asset/audios/samouth/ជញ្ជីងស្នេហា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ជីវិតកម្មករត្បូង",
		path: "/asset/audios/samouth/ជីវិតកម្មករត្បូង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ជីវិតខ្ញុំសម្រាប់អ្នក",
		path: "/asset/audios/samouth/ជីវិតខ្ញុំសម្រាប់អ្នក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ជូតទឹកភ្នែកទៅអូន",
		path: "/asset/audios/samouth/ជូតទឹកភ្នែកទៅអូន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ជួបគ្នានៅបាត់ដំបង",
		path: "/asset/audios/samouth/ជួបគ្នានៅបាត់ដំបង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ជើងមេឃពណ៌ស",
		path: "/asset/audios/samouth/ជើងមេឃពណ៌ស.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ជើងមេឃពណ៌ស្វាយ",
		path: "/asset/audios/samouth/ជើងមេឃពណ៌ស្វាយ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ជឿចិត្តទាំងស្រុង",
		path: "/asset/audios/samouth/ជឿចិត្តទាំងស្រុង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ជ័យលក់នំបញ្ចុក",
		path: "/asset/audios/samouth/ជ័យលក់នំបញ្ចុក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ជ្រលងសេកមាស",
		path: "/asset/audios/samouth/ជ្រលងសេកមាស.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ដប់ឆ្នាំដើររើសប្រពន្ធ",
		path: "/asset/audios/samouth/ដប់ឆ្នាំដើររើសប្រពន្ធ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ដីរៀមគ្រាមចិត្ត",
		path: "/asset/audios/samouth/ដីរៀមគ្រាមចិត្ត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ដូចជាប្រហែលមុខ",
		path: "/asset/audios/samouth/ដូចជាប្រហែលមុខ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ដួងនេត្រា",
		path: "/asset/audios/samouth/ដួងនេត្រា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ដើមដូងព្រលឹងស្នេហ៍",
		path: "/asset/audios/samouth/ដើមដូងព្រលឹងស្នេហ៍.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ដំបៅដួងចិត្ត",
		path: "/asset/audios/samouth/ដំបៅដួងចិត្ត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "តូចអើយស្រីតូច",
		path: "/asset/audios/samouth/តូចអើយស្រីតូច.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "តើមាននរណាខ្លះដូចខ្ញុំ",
		path: "/asset/audios/samouth/តើមាននរណាខ្លះដូចខ្ញុំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ត្រជាក់ចិត្ត",
		path: "/asset/audios/samouth/ត្រជាក់ចិត្ត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ត្រពាំងពាយ",
		path: "/asset/audios/samouth/ត្រពាំងពាយ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ថើបជួស",
		path: "/asset/audios/samouth/ថើបជួស.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ថើប១០០០ដង",
		path: "/asset/audios/samouth/ថើប១០០០ដង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ថ្ងៃច័ន្ទអនុស្សា",
		path: "/asset/audios/samouth/ថ្ងៃច័ន្ទអនុស្សា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ថ្ងៃណាមានសង្ឃឹម",
		path: "/asset/audios/samouth/ថ្ងៃណាមានសង្ឃឹម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ថ្ងៃ១២កក្កដា",
		path: "/asset/audios/samouth/ថ្ងៃ១២កក្កដា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ថ្នមស្នេហ៍",
		path: "/asset/audios/samouth/ថ្នមស្នេហ៍.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ថ្មគោលស្រមោលស្នេហ៍",
		path: "/asset/audios/samouth/ថ្មគោលស្រមោលស្នេហ៍.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ទឹកជ្រោះធ្លាក់លើថ្ម",
		path: "/asset/audios/samouth/ទឹកជ្រោះធ្លាក់លើថ្ម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ទឹកជ្រោះអនុស្សាវរីយ៍",
		path: "/asset/audios/samouth/ទឹកជ្រោះអនុស្សាវរីយ៍.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ទឹកភ្នែកអូនស្រក់លើទ្រូងបង",
		path: "/asset/audios/samouth/ទឹកភ្នែកអូនស្រក់លើទ្រូងបង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ទួលគោកទួលកម្ម",
		path: "/asset/audios/samouth/ទួលគោកទួលកម្ម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ទេពធីតាក្នុងសុបិន",
		path: "/asset/audios/samouth/ទេពធីតាក្នុងសុបិន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ទេវីកម្ពុជា",
		path: "/asset/audios/samouth/ទេវីកម្ពុជា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ទេសចរណ៍នៅកី្តស្រមៃ",
		path: "/asset/audios/samouth/ទេសចរណ៍នៅកី្តស្រមៃ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ទៅណាមកណានាង",
		path: "/asset/audios/samouth/ទៅណាមកណានាង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ទៅភ្ជុំនៅអារញ្ញ",
		path: "/asset/audios/samouth/ទៅភ្ជុំនៅអារញ្ញ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ទៅលេងបុណ្យភ្ជុំ",
		path: "/asset/audios/samouth/ទៅលេងបុណ្យភ្ជុំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ទៅលេងរាំវង់",
		path: "/asset/audios/samouth/ទៅលេងរាំវង់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ទៅលេងអូរជ្រៅ",
		path: "/asset/audios/samouth/ទៅលេងអូរជ្រៅ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ទំនួញចាបមាស",
		path: "/asset/audios/samouth/ទំនួញចាបមាស.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ទំនួញស្តេចនាគ (សុព័រទេវី)",
		path: "/asset/audios/samouth/ទំនួញស្តេចនាគ (សុព័រទេវី).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ទំនួញហ្គីតា",
		path: "/asset/audios/samouth/ទំនួញហ្គីតា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ទ្រាំដល់ផុតកម្ម",
		path: "/asset/audios/samouth/ទ្រាំដល់ផុតកម្ម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ធាត់ឬស្គម",
		path: "/asset/audios/samouth/ធាត់ឬស្គម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ធូបបីសរសៃ",
		path: "/asset/audios/samouth/ធូបបីសរសៃ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ធ្វើអ្វីក៏ធ្វើដែរ",
		path: "/asset/audios/samouth/ធ្វើអ្វីក៏ធ្វើដែរ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "នាងគ ក៏ខ្ញុំស្រលាញ់",
		path: "/asset/audios/samouth/នាងគ ក៏ខ្ញុំស្រលាញ់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "នារីបរទេសឬ",
		path: "/asset/audios/samouth/នារីបរទេសឬ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "នារីសម័យឆ្នាំ72",
		path: "/asset/audios/samouth/នារីសម័យឆ្នាំ72.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "នឹកគ្រប់ពេលវេលា",
		path: "/asset/audios/samouth/នឹកគ្រប់ពេលវេលា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "នឹកគ្រប់វេលា",
		path: "/asset/audios/samouth/នឹកគ្រប់វេលា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "នឹកឃើញ នឹកឃើញ",
		path: "/asset/audios/samouth/នឹកឃើញ នឹកឃើញ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "នឹកឃើញទាំងអស់",
		path: "/asset/audios/samouth/នឹកឃើញទាំងអស់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "នេះគឺកម្មអូន",
		path: "/asset/audios/samouth/នេះគឺកម្មអូន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "នៅកំលោះយូរៗទៅ",
		path: "/asset/audios/samouth/នៅកំលោះយូរៗទៅ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "នៅតែសង្ឃឹម",
		path: "/asset/audios/samouth/នៅតែសង្ឃឹម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "នៅតែស្រលាញ់",
		path: "/asset/audios/samouth/នៅតែស្រលាញ់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បក្សាស្លាបដែក",
		path: "/asset/audios/samouth/បក្សាស្លាបដែក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បងចាំមាសស្ងួន",
		path: "/asset/audios/samouth/បងចាំមាសស្ងួន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បងចេះតែ Twist",
		path: "/asset/audios/samouth/បងចេះតែ Twist.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បងច្រៀងនាងយំ",
		path: "/asset/audios/samouth/បងច្រៀងនាងយំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បងនាំខ្មៅរត់",
		path: "/asset/audios/samouth/បងនាំខ្មៅរត់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បងនៅតែចាំ",
		path: "/asset/audios/samouth/បងនៅតែចាំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បងស្គមឣស់ហើយអូន",
		path: "/asset/audios/samouth/បងស្គមឣស់ហើយអូន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បងស្រលាញ់តែអូន",
		path: "/asset/audios/samouth/បងស្រលាញ់តែអូន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បងស្រលាញ់ស្រីតូច",
		path: "/asset/audios/samouth/បងស្រលាញ់ស្រីតូច.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បងស្រលាញ់អូនម្នាក់",
		path: "/asset/audios/samouth/បងស្រលាញ់អូនម្នាក់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បង្គងកម្ម",
		path: "/asset/audios/samouth/បង្គងកម្ម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បណ្តាំតាមខ្យល់",
		path: "/asset/audios/samouth/បណ្តាំតាមខ្យល់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បណ្តាំតាមសារិកា",
		path: "/asset/audios/samouth/បណ្តាំតាមសារិកា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បណ្តាំហោរា",
		path: "/asset/audios/samouth/បណ្តាំហោរា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បណ្តែតក្បូនលេង",
		path: "/asset/audios/samouth/បណ្តែតក្បូនលេង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បាត់ដំបងមានអ្វីឆ្ងាញ់",
		path: "/asset/audios/samouth/បាត់ដំបងមានអ្វីឆ្ងាញ់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បិសាចក្រមុំ",
		path: "/asset/audios/samouth/បិសាចក្រមុំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បុប្ផាកោះរំដួល",
		path: "/asset/audios/samouth/បុប្ផាកោះរំដួល.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បុប្ផាកំពង់ហាវ",
		path: "/asset/audios/samouth/បុប្ផាកំពង់ហាវ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បុប្ផាឈៀងម៉ៃ",
		path: "/asset/audios/samouth/បុប្ផាឈៀងម៉ៃ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បុប្ផាលាក់ខ្លួន",
		path: "/asset/audios/samouth/បុប្ផាលាក់ខ្លួន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "បើនឹងព្រាត់",
		path: "/asset/audios/samouth/បើនឹងព្រាត់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ប៉ៃលិនដួងចិត្ត",
		path: "/asset/audios/samouth/ប៉ៃលិនដួងចិត្ត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ប៉ះដៃបានហើយ",
		path: "/asset/audios/samouth/ប៉ះដៃបានហើយ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ប្រគល់ដួងចិត្ត",
		path: "/asset/audios/samouth/ប្រគល់ដួងចិត្ត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ប្រយ័ត្នបាបណា",
		path: "/asset/audios/samouth/ប្រយ័ត្នបាបណា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ប្រយ័ត្នពៀរឆ្លង",
		path: "/asset/audios/samouth/ប្រយ័ត្នពៀរឆ្លង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ប្រយ័ត្នឲ្យបងគេងក្រៅមុង",
		path: "/asset/audios/samouth/ប្រយ័ត្នឲ្យបងគេងក្រៅមុង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ប្រលោមសុំស្នេហ៍",
		path: "/asset/audios/samouth/ប្រលោមសុំស្នេហ៍.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ប្រវ៉ាស់ស្រែម្តង",
		path: "/asset/audios/samouth/ប្រវ៉ាស់ស្រែម្តង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ប្រស្នា4ខ",
		path: "/asset/audios/samouth/ប្រស្នា4ខ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ផាត់ជាយបណ្តូលចិត្ត",
		path: "/asset/audios/samouth/ផាត់ជាយបណ្តូលចិត្ត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ផ្កាកម្ពុជា",
		path: "/asset/audios/samouth/ផ្កាកម្ពុជា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ផ្កាកំភ្លឹង",
		path: "/asset/audios/samouth/ផ្កាកំភ្លឹង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ផ្កាក្រវ៉ាន់",
		path: "/asset/audios/samouth/ផ្កាក្រវ៉ាន់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ផ្កាម្រុំ",
		path: "/asset/audios/samouth/ផ្កាម្រុំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ផ្កាយព្រឹក",
		path: "/asset/audios/samouth/ផ្កាយព្រឹក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ផ្កាយព្រះអង្គារ (ភ្លេងចាស់)",
		path: "/asset/audios/samouth/ផ្កាយព្រះអង្គារ (ភ្លេងចាស់).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ផ្កាយព្រះអង្គារ (ភ្លេងថ្មី)",
		path: "/asset/audios/samouth/ផ្កាយព្រះអង្គារ (ភ្លេងថ្មី).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ផ្ការីកក្នុងចិត្ត",
		path: "/asset/audios/samouth/ផ្ការីកក្នុងចិត្ត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ផ្ការីកផ្ការោយ",
		path: "/asset/audios/samouth/ផ្ការីកផ្ការោយ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ផ្កាអង្គារបុស្ស",
		path: "/asset/audios/samouth/ផ្កាអង្គារបុស្ស.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ផ្កាអើយផ្កាថ្កុល",
		path: "/asset/audios/samouth/ផ្កាអើយផ្កាថ្កុល.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ផ្គរលាន់ឯត្បូង",
		path: "/asset/audios/samouth/ផ្គរលាន់ឯត្បូង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ផ្ញើប្រាណលើផ្កាឈូកស",
		path: "/asset/audios/samouth/ផ្ញើប្រាណលើផ្កាឈូកស.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ផ្សងជួបគ្រប់ជាតិ",
		path: "/asset/audios/samouth/ផ្សងជួបគ្រប់ជាតិ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ពលទោអភ័ព្វ",
		path: "/asset/audios/samouth/ពលទោអភ័ព្វ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ពេលភ្លៀងនាងទៅណា",
		path: "/asset/audios/samouth/ពេលភ្លៀងនាងទៅណា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ពេលអូនឡើងយន្ដហោះ",
		path: "/asset/audios/samouth/ពេលអូនឡើងយន្ដហោះ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ពោះម៉ាយខាន់ស្លា",
		path: "/asset/audios/samouth/ពោះម៉ាយខាន់ស្លា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ព្យុះជីវិត",
		path: "/asset/audios/samouth/ព្យុះជីវិត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ព្រលិតបឹងស្រី",
		path: "/asset/audios/samouth/ព្រលិតបឹងស្រី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ព្រាត់ទាំងសង្ឃឹម",
		path: "/asset/audios/samouth/ព្រាត់ទាំងសង្ឃឹម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ព្រាត់ភ្នំសំពៅ",
		path: "/asset/audios/samouth/ព្រាត់ភ្នំសំពៅ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ព្រួញភ្នែកអូន",
		path: "/asset/audios/samouth/ព្រួញភ្នែកអូន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ព្រួយរបស់បង",
		path: "/asset/audios/samouth/ព្រួយរបស់បង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ព្រែកឯងអស់សង្ឃឹម",
		path: "/asset/audios/samouth/ព្រែកឯងអស់សង្ឃឹម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ព្រៃអើយ ព្រៃជ្រៅ",
		path: "/asset/audios/samouth/ព្រៃអើយ ព្រៃជ្រៅ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ព្រៃឯកើត",
		path: "/asset/audios/samouth/ព្រៃឯកើត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ព្រោះតែអូន",
		path: "/asset/audios/samouth/ព្រោះតែអូន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ព្រោះបងធ្លាប់ស្រលាញ់",
		path: "/asset/audios/samouth/ព្រោះបងធ្លាប់ស្រលាញ់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ព្រះពាយផាត់",
		path: "/asset/audios/samouth/ព្រះពាយផាត់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ភក្ត្រាស្រទន់",
		path: "/asset/audios/samouth/ភក្ត្រាស្រទន់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ភរិយាទាហាន",
		path: "/asset/audios/samouth/ភរិយាទាហាន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ភូមិខ្មែរភូមិថ្មី",
		path: "/asset/audios/samouth/ភូមិខ្មែរភូមិថ្មី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ភ្នែកខូច",
		path: "/asset/audios/samouth/ភ្នែកខូច.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ភ្នែកអូនមានអ្វី",
		path: "/asset/audios/samouth/ភ្នែកអូនមានអ្វី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ភ្នំតូចភ្នំធំ",
		path: "/asset/audios/samouth/ភ្នំតូចភ្នំធំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ភ្លើងគ្មានផ្សែង",
		path: "/asset/audios/samouth/ភ្លើងគ្មានផ្សែង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ភ្លៀងធ្លាក់លើបង្អួច",
		path: "/asset/audios/samouth/ភ្លៀងធ្លាក់លើបង្អួច.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ភ្លេងមច្ចុរាជ",
		path: "/asset/audios/samouth/ភ្លេងមច្ចុរាជ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មករាំនឹងខ្ញុំ",
		path: "/asset/audios/samouth/មករាំនឹងខ្ញុំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មនុស្សចិត្តផ្លែល្វា",
		path: "/asset/audios/samouth/មនុស្សចិត្តផ្លែល្វា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មន្តស្នេហ៏ស្រីខ្មៅ",
		path: "/asset/audios/samouth/មន្តស្នេហ៏ស្រីខ្មៅ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មន្ថាមាសបង",
		path: "/asset/audios/samouth/មន្ថាមាសបង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មាននិស្ស័យទើបយើងជួបគ្នា",
		path: "/asset/audios/samouth/មាននិស្ស័យទើបយើងជួបគ្នា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មានលុយមានស្នេហ៍",
		path: "/asset/audios/samouth/មានលុយមានស្នេហ៍.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មាន់រងាវ",
		path: "/asset/audios/samouth/មាន់រងាវ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មាសទឹកដប់",
		path: "/asset/audios/samouth/មាសទឹកដប់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មាសទឹកប្រាំបី",
		path: "/asset/audios/samouth/មាសទឹកប្រាំបី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មិនចុះអេតាស៊ីវិល",
		path: "/asset/audios/samouth/មិនចុះអេតាស៊ីវិល.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មិនដែលឃើញសោះ",
		path: "/asset/audios/samouth/មិនដែលឃើញសោះ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មិនយល់ទេ",
		path: "/asset/audios/samouth/មិនយល់ទេ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មិនលក់ទេស្នេហ៍ (ភ្នែកបងងងឹត)",
		path: "/asset/audios/samouth/មិនលក់ទេស្នេហ៍ (ភ្នែកបងងងឹត).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មុន្នីខេរាអាកាសយាន្ត",
		path: "/asset/audios/samouth/មុន្នីខេរាអាកាសយាន្ត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មួយកំផ្លៀងអនុស្សាវរីយ៍ (ភ្លេងចាស់)",
		path: "/asset/audios/samouth/មួយកំផ្លៀងអនុស្សាវរីយ៍ (ភ្លេងចាស់).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មួយកំផ្លៀងអនុស្សាវរីយ៍ (ភ្លេងថ្មី)",
		path: "/asset/audios/samouth/មួយកំផ្លៀងអនុស្សាវរីយ៍ (ភ្លេងថ្មី).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មួយម៉ឺនអាល័យ",
		path: "/asset/audios/samouth/មួយម៉ឺនអាល័យ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មើលមេឃមើលស្រី",
		path: "/asset/audios/samouth/មើលមេឃមើលស្រី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មេឃអើយកុំភ្លៀង",
		path: "/asset/audios/samouth/មេឃអើយកុំភ្លៀង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "មេម៉ាយថ្មីៗ",
		path: "/asset/audios/samouth/មេម៉ាយថ្មីៗ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ម៉ាឡាមាសបង",
		path: "/asset/audios/samouth/ម៉ាឡាមាសបង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ម្ចាស់ក្លិននៅទីណា",
		path: "/asset/audios/samouth/ម្ចាស់ក្លិននៅទីណា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ម្តេចហ៊ានសួរខ្ញុំ",
		path: "/asset/audios/samouth/ម្តេចហ៊ានសួរខ្ញុំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ម្លិះរួតភូមិថ្មី",
		path: "/asset/audios/samouth/ម្លិះរួតភូមិថ្មី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "យប់ណាក៏ដូចយប់ណា",
		path: "/asset/audios/samouth/យប់ណាក៏ដូចយប់ណា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "យប់១២កើត",
		path: "/asset/audios/samouth/យប់១២កើត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "យល់ចិត្តរៀមផង",
		path: "/asset/audios/samouth/យល់ចិត្តរៀមផង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "យីហ៊ុបប៉ៃលិន",
		path: "/asset/audios/samouth/យីហ៊ុបប៉ៃលិន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "យំដើម្បីឈប់យំ",
		path: "/asset/audios/samouth/យំដើម្បីឈប់យំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "យ៉ាងណាទៅស្នេហា",
		path: "/asset/audios/samouth/យ៉ាងណាទៅស្នេហា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រតនាវង្ស សុគន្ធារស",
		path: "/asset/audios/samouth/រតនាវង្ស សុគន្ធារស.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រយពាន់សារភាព",
		path: "/asset/audios/samouth/រយពាន់សារភាព.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រសស្នេហ៍ឧត្តម",
		path: "/asset/audios/samouth/រសស្នេហ៍ឧត្តម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រសើបណាស់",
		path: "/asset/audios/samouth/រសើបណាស់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រសៀលគងភ្នំ",
		path: "/asset/audios/samouth/រសៀលគងភ្នំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រាត្រីដែលបងនឹកមិនភ្លេច",
		path: "/asset/audios/samouth/រាត្រីដែលបងនឹកមិនភ្លេច.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រាត្រីដែលបានជួបភ័ក្រ្ត",
		path: "/asset/audios/samouth/រាត្រីដែលបានជួបភ័ក្រ្ត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រាត្រីរនោច",
		path: "/asset/audios/samouth/រាត្រីរនោច.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រាប់ផ្កាយ (អូនមើលដួងតារា)",
		path: "/asset/audios/samouth/រាប់ផ្កាយ (អូនមើលដួងតារា).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រាល់ពេលជួបអូន",
		path: "/asset/audios/samouth/រាល់ពេលជួបអូន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រាហ៊ូចាប់ច័ន្ទ",
		path: "/asset/audios/samouth/រាហ៊ូចាប់ច័ន្ទ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រាំក្បាច់សារ៉ាវ៉ាន់",
		path: "/asset/audios/samouth/រាំក្បាច់សារ៉ាវ៉ាន់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រាំតាមចិត្តស្ម័គ្រ",
		path: "/asset/audios/samouth/រាំតាមចិត្តស្ម័គ្រ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រាំវង់ឯករាជ្យ",
		path: "/asset/audios/samouth/រាំវង់ឯករាជ្យ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រូបពីរជីវិតមួយ",
		path: "/asset/audios/samouth/រូបពីរជីវិតមួយ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រូបអូនហ្នឹងហើយ",
		path: "/asset/audios/samouth/រូបអូនហ្នឹងហើយ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រឿងសេកដូចខ្ញុំ",
		path: "/asset/audios/samouth/រឿងសេកដូចខ្ញុំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រៀមច្បងគ្រួសារ",
		path: "/asset/audios/samouth/រៀមច្បងគ្រួសារ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រំដួលដងស្ទឹងសង្កែ",
		path: "/asset/audios/samouth/រំដួលដងស្ទឹងសង្កែ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រំដួលភ្នំសំពៅ",
		path: "/asset/audios/samouth/រំដួលភ្នំសំពៅ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រំដួលសុរិន",
		path: "/asset/audios/samouth/រំដួលសុរិន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "រំដួលអង្គរ",
		path: "/asset/audios/samouth/រំដួលអង្គរ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "លាពៅសំណព្វ",
		path: "/asset/audios/samouth/លាពៅសំណព្វ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "លាស្រីទៅបំរើជាតិ",
		path: "/asset/audios/samouth/លាស្រីទៅបំរើជាតិ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "លាស្រីសាវា",
		path: "/asset/audios/samouth/លាស្រីសាវា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "លាហើយសូម៉ាត្រា",
		path: "/asset/audios/samouth/លាហើយសូម៉ាត្រា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "លីនជូ មាសបង",
		path: "/asset/audios/samouth/លីនជូ មាសបង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "លុះក្ស័យបងស្មោះ",
		path: "/asset/audios/samouth/លុះក្ស័យបងស្មោះ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "លុះលង់ក្រោមធរណី",
		path: "/asset/audios/samouth/លុះលង់ក្រោមធរណី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "លួងលោមចៅស្រទបចេក",
		path: "/asset/audios/samouth/លួងលោមចៅស្រទបចេក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "លួចស្នេហ៍លួចទុក្ខ",
		path: "/asset/audios/samouth/លួចស្នេហ៍លួចទុក្ខ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ល្មមហើយណាស្រី",
		path: "/asset/audios/samouth/ល្មមហើយណាស្រី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ល្វាចេកព្រាត់គូ",
		path: "/asset/audios/samouth/ល្វាចេកព្រាត់គូ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "វាចារយុទ្ធជនឃុំគគីរធំ",
		path: "/asset/audios/samouth/វាចារយុទ្ធជនឃុំគគីរធំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "វាលស្រីស្រណោះ",
		path: "/asset/audios/samouth/វាលស្រីស្រណោះ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "វាសនាខ្សែទឹកឈូ",
		path: "/asset/audios/samouth/វាសនាខ្សែទឹកឈូ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "វិលវិញមកអូន",
		path: "/asset/audios/samouth/វិលវិញមកអូន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "វិស្សមកាលក្រុងកែប",
		path: "/asset/audios/samouth/វិស្សមកាលក្រុងកែប.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "វីយ៉ូឡែត្តា",
		path: "/asset/audios/samouth/វីយ៉ូឡែត្តា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សក់ក្រង",
		path: "/asset/audios/samouth/សក់ក្រង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សក់ខ្មៅរលោង",
		path: "/asset/audios/samouth/សក់ខ្មៅរលោង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សក់វែងម្ដេចមិនអ៊ុត",
		path: "/asset/audios/samouth/សក់វែងម្ដេចមិនអ៊ុត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សក់វែងអន្លាយ",
		path: "/asset/audios/samouth/សក់វែងអន្លាយ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សក្រវាផ្កាកំភ្លឹង",
		path: "/asset/audios/samouth/សក្រវាផ្កាកំភ្លឹង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សង្ឃឹមតិចៗ",
		path: "/asset/audios/samouth/សង្ឃឹមតិចៗ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សត្វចាបយំចេប",
		path: "/asset/audios/samouth/សត្វចាបយំចេប.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សន្ទូចអត់នុយ",
		path: "/asset/audios/samouth/សន្ទូចអត់នុយ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សន្យាបីថ្ងៃ",
		path: "/asset/audios/samouth/សន្យាបីថ្ងៃ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សម្បថមុខព្រះ",
		path: "/asset/audios/samouth/សម្បថមុខព្រះ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សម្រស់នាងលក្ខស៊្មី",
		path: "/asset/audios/samouth/សម្រស់នាងលក្ខស៊្មី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សម្រស់បុប្ផាកោះកុង",
		path: "/asset/audios/samouth/សម្រស់បុប្ផាកោះកុង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សម្រស់អ្នកគ្រូក្រមុំ",
		path: "/asset/audios/samouth/សម្រស់អ្នកគ្រូក្រមុំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សម្រែកឈាមខ្មែរ",
		path: "/asset/audios/samouth/សម្រែកឈាមខ្មែរ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សម្លេងទាវឯក",
		path: "/asset/audios/samouth/សម្លេងទាវឯក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សាងផ្នួសចុះអូន",
		path: "/asset/audios/samouth/សាងផ្នួសចុះអូន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សាងផ្នួសលាងកម្ម",
		path: "/asset/audios/samouth/សាងផ្នួសលាងកម្ម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សុបិនចម្លែក",
		path: "/asset/audios/samouth/សុបិនចម្លែក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សុបិនបីយប់",
		path: "/asset/audios/samouth/សុបិនបីយប់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សុបិន្តសួគ៌ា (ភ័ព្វព្រេងវាសនា)",
		path: "/asset/audios/samouth/សុបិន្តសួគ៌ា (ភ័ព្វព្រេងវាសនា).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សុរិយាកំពង់ប្រាក់",
		path: "/asset/audios/samouth/សុរិយាកំពង់ប្រាក់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សុរិយាផ្សងស្នេហ៍",
		path: "/asset/audios/samouth/សុរិយាផ្សងស្នេហ៍.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សុរិយាល្ងាចថ្ងៃ",
		path: "/asset/audios/samouth/សុរិយាល្ងាចថ្ងៃ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សុវណ្ណតែងអន",
		path: "/asset/audios/samouth/សុវណ្ណតែងអន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សុំខ្ចីគូរាំ",
		path: "/asset/audios/samouth/សុំខ្ចីគូរាំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សុំផ្ទះអូនជ្រក",
		path: "/asset/audios/samouth/សុំផ្ទះអូនជ្រក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សូរិយាអស្តង្គត",
		path: "/asset/audios/samouth/សូរិយាអស្តង្គត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សែនសប្បាយ",
		path: "/asset/audios/samouth/សែនសប្បាយ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សែនស្រលាញ់",
		path: "/asset/audios/samouth/សែនស្រលាញ់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សោភីនីមាសស្នេហ៍",
		path: "/asset/audios/samouth/សោភីនីមាសស្នេហ៍.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សំដីមិនទៀង",
		path: "/asset/audios/samouth/សំដីមិនទៀង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សំណព្វចិត្ត",
		path: "/asset/audios/samouth/សំណព្វចិត្ត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សំណៅកាកី",
		path: "/asset/audios/samouth/សំណៅកាកី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សំបុត្រចាក់សោរទុក",
		path: "/asset/audios/samouth/សំបុត្រចាក់សោរទុក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "សំបុត្រចុងក្រោយ",
		path: "/asset/audios/samouth/សំបុត្រចុងក្រោយ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្តាយក្លិនដែលឃ្លាត",
		path: "/asset/audios/samouth/ស្តាយក្លិនដែលឃ្លាត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្តាយឆោម",
		path: "/asset/audios/samouth/ស្តាយឆោម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្តាយរាងអូនណាស់",
		path: "/asset/audios/samouth/ស្តាយរាងអូនណាស់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្តាយស្នេហ៍អភ័ព្វ",
		path: "/asset/audios/samouth/ស្តាយស្នេហ៍អភ័ព្វ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្ទឹងកែវ",
		path: "/asset/audios/samouth/ស្ទឹងកែវ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្នេហាឆ្មា និង កណ្តុរ",
		path: "/asset/audios/samouth/ស្នេហាឆ្មា និង កណ្តុរ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្នេហាព្រះលក្សិនវង្ស",
		path: "/asset/audios/samouth/ស្នេហាព្រះលក្សិនវង្ស.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្នេហ៍ក្រមុំចាស់",
		path: "/asset/audios/samouth/ស្នេហ៍ក្រមុំចាស់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្នេហ៍ឆ្លងវេហា (ខ្មែរ)",
		path: "/asset/audios/samouth/ស្នេហ៍ឆ្លងវេហា (ខ្មែរ).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្នេហ៍ឆ្លងវេហា (ថៃ)",
		path: "/asset/audios/samouth/ស្នេហ៍ឆ្លងវេហា (ថៃ).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្នេហ៍ដូចជើងមេឃ",
		path: "/asset/audios/samouth/ស្នេហ៍ដូចជើងមេឃ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្នេហ៍បាត់សម្រស់",
		path: "/asset/audios/samouth/ស្នេហ៍បាត់សម្រស់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្នេហ៍យើងនៅដដែល",
		path: "/asset/audios/samouth/ស្នេហ៍យើងនៅដដែល.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្នេហ៍លើពសុធា (ចិត្តបងសព្វថ្ងៃ)",
		path: "/asset/audios/samouth/ស្នេហ៍លើពសុធា (ចិត្តបងសព្វថ្ងៃ).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្នេហ៍លើអាកាស",
		path: "/asset/audios/samouth/ស្នេហ៍លើអាកាស.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្នេហ៍ស្រីអន",
		path: "/asset/audios/samouth/ស្នេហ៍ស្រីអន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្នេហ៍ឥតន័យ",
		path: "/asset/audios/samouth/ស្នេហ៍ឥតន័យ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្ពានឈើអភ័ព្វ",
		path: "/asset/audios/samouth/ស្ពានឈើអភ័ព្វ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្ពានអូរតាគី",
		path: "/asset/audios/samouth/ស្ពានអូរតាគី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្មាមស្នេហ៍សមុទ្ររាម",
		path: "/asset/audios/samouth/ស្មាមស្នេហ៍សមុទ្ររាម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រក់ទឹកភ្នែកព្រោះអ្វី (យំបីរាត្រី)",
		path: "/asset/audios/samouth/ស្រក់ទឹកភ្នែកព្រោះអ្វី (យំបីរាត្រី).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រណោះក្លិនស្រី",
		path: "/asset/audios/samouth/ស្រណោះក្លិនស្រី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រណោះគ្រាកន្លង",
		path: "/asset/audios/samouth/ស្រណោះគ្រាកន្លង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រណោះភូមិដើមល្វា",
		path: "/asset/audios/samouth/ស្រណោះភូមិដើមល្វា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រលាញ់ត្រូវហ៊ាន",
		path: "/asset/audios/samouth/ស្រលាញ់ត្រូវហ៊ាន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រលាញ់អូន៣៦៥ឆ្នាំ",
		path: "/asset/audios/samouth/ស្រលាញ់អូន៣៦៥ឆ្នាំ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រស់រកមិនបាន",
		path: "/asset/audios/samouth/ស្រស់រកមិនបាន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រឡាញ់បងទៅ",
		path: "/asset/audios/samouth/ស្រឡាញ់បងទៅ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រឡាញ់បងទៅអូន",
		path: "/asset/audios/samouth/ស្រឡាញ់បងទៅអូន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រអែមខ្មៅស្ទើរ",
		path: "/asset/audios/samouth/ស្រអែមខ្មៅស្ទើរ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រអែមដងស្ទឹងសង្កែ",
		path: "/asset/audios/samouth/ស្រអែមដងស្ទឹងសង្កែ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រអែមផល្លា",
		path: "/asset/audios/samouth/ស្រអែមផល្លា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រអែមស្រីស្រស់",
		path: "/asset/audios/samouth/ស្រអែមស្រីស្រស់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រីចិត្តមិនស្មោះ",
		path: "/asset/audios/samouth/ស្រីចិត្តមិនស្មោះ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រីតូច",
		path: "/asset/audios/samouth/ស្រីតូច.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រីតូចច្រឡឹង",
		path: "/asset/audios/samouth/ស្រីតូចច្រឡឹង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រីតូចទ្រមែ",
		path: "/asset/audios/samouth/ស្រីតូចទ្រមែ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រីសម័យឥឡូវ",
		path: "/asset/audios/samouth/ស្រីសម័យឥឡូវ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រីស្រស់ប្រុសស្អាត",
		path: "/asset/audios/samouth/ស្រីស្រស់ប្រុសស្អាត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្រីស្រអែម",
		path: "/asset/audios/samouth/ស្រីស្រអែម.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្លឹកឈើចាកមែក",
		path: "/asset/audios/samouth/ស្លឹកឈើចាកមែក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្លឹកដូងស្លឹកចាក",
		path: "/asset/audios/samouth/ស្លឹកដូងស្លឹកចាក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្អប់និងស្រលាញ់",
		path: "/asset/audios/samouth/ស្អប់និងស្រលាញ់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ស្អែកការប្រពន្ធ",
		path: "/asset/audios/samouth/ស្អែកការប្រពន្ធ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ហេតុអ្វីស្រីភបង (ថ្ងៃមុនម្តេចបងថើបអូន)",
		path: "/asset/audios/samouth/ហេតុអ្វីស្រីភបង (ថ្ងៃមុនម្តេចបងថើបអូន).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អកបឹងកន្សែង",
		path: "/asset/audios/samouth/អកបឹងកន្សែង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អង្ករស្រូវថ្មី",
		path: "/asset/audios/samouth/អង្ករស្រូវថ្មី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អង្គលីមារ",
		path: "/asset/audios/samouth/អង្គលីមារ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អង្គុញកោយបី",
		path: "/asset/audios/samouth/អង្គុញកោយបី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អង្វរកេសរស្រី",
		path: "/asset/audios/samouth/អង្វរកេសរស្រី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អញ្ជើញ អញ្ជើញ",
		path: "/asset/audios/samouth/អញ្ជើញ អញ្ជើញ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អនិច្ចាក្រឡាហោមគង់",
		path: "/asset/audios/samouth/អនិច្ចាក្រឡាហោមគង់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អនុភាពនៃពុកមាត់",
		path: "/asset/audios/samouth/អនុភាពនៃពុកមាត់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អនុស្សាវរីយ៌៣ខែត្រ",
		path: "/asset/audios/samouth/អនុស្សាវរីយ៌៣ខែត្រ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អនុស្សាវរីយ៍សេកសក",
		path: "/asset/audios/samouth/អនុស្សាវរីយ៍សេកសក.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អាចារ្យបាំងឈើ",
		path: "/asset/audios/samouth/អាចារ្យបាំងឈើ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អានី",
		path: "/asset/audios/samouth/អានី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អារសាច់ជូនម្តាយ",
		path: "/asset/audios/samouth/អារសាច់ជូនម្តាយ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អាវយ័នកេរម៉ែ (ស្អែកលាម៉ែចូលធ្វើទាហាន)",
		path: "/asset/audios/samouth/អាវយ័នកេរម៉ែ (ស្អែកលាម៉ែចូលធ្វើទាហាន).mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អាឡូអូន អាឡូបង",
		path: "/asset/audios/samouth/អាឡូអូន អាឡូបង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អូ!យេៗ",
		path: "/asset/audios/samouth/អូ!យេៗ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អូនកូនអ្នកណា",
		path: "/asset/audios/samouth/អូនកូនអ្នកណា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អូនកើតឆ្នាំអី",
		path: "/asset/audios/samouth/អូនកើតឆ្នាំអី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អូនគ្មានមេត្តា",
		path: "/asset/audios/samouth/អូនគ្មានមេត្តា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អូនរៀបការចុះ",
		path: "/asset/audios/samouth/អូនរៀបការចុះ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អ្នកណាឲ្យគូរ",
		path: "/asset/audios/samouth/អ្នកណាឲ្យគូរ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "អ្នកប្រដាល់សេរី",
		path: "/asset/audios/samouth/អ្នកប្រដាល់សេរី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ឤស៊ីណាស្នេហាបង",
		path: "/asset/audios/samouth/ឤស៊ីណាស្នេហាបង.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ឧត្តមដួងចិត្ត",
		path: "/asset/audios/samouth/ឧត្តមដួងចិត្ត.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ឧត្តមភរិយាទាហាន",
		path: "/asset/audios/samouth/ឧត្តមភរិយាទាហាន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ឧត្តមស្នេហា",
		path: "/asset/audios/samouth/ឧត្តមស្នេហា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ឯណាទៅឋានសួគ៌",
		path: "/asset/audios/samouth/ឯណាទៅឋានសួគ៌.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ឯណាព្រហ្មចារីយ៍",
		path: "/asset/audios/samouth/ឯណាព្រហ្មចារីយ៍.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ឱ! ធម្មជាតិអើយ",
		path: "/asset/audios/samouth/ឱ! ធម្មជាតិអើយ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ឱ! ផ្សែងបារី",
		path: "/asset/audios/samouth/ឱ! ផ្សែងបារី.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ឱ!ដួងតារាខ្ញុំអើយ",
		path: "/asset/audios/samouth/ឱ!ដួងតារាខ្ញុំអើយ.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ឱស្ទឹងពោធិសាត់",
		path: "/asset/audios/samouth/ឱស្ទឹងពោធិសាត់.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ឲ្យតែបាន",
		path: "/asset/audios/samouth/ឲ្យតែបាន.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
	{
		name: "ឳ! ភូមិដើមល្វា",
		path: "/asset/audios/samouth/ឳ! ភូមិដើមល្វា.mp3",
		img: "/asset/images/samouth.jpg",
		singer: "Sinn Sisamouth"
	},
];


// All functions


// function load the track
function load_track(index_no){
	clearInterval(timer);
	reset_slider();

	track.src = All_song[index_no].path;
	title.innerHTML = All_song[index_no].name;	
	track_image.src = All_song[index_no].img;
    artist.innerHTML = All_song[index_no].singer;
    track.load();

	timer = setInterval(range_slider ,1000);
	total.innerHTML = All_song.length;
	present.innerHTML = index_no + 1;
}

load_track(index_no);


//mute sound function
function mute_sound(){
	track.volume = 0;
	volume.value = 0;
	volume_show.innerHTML = 0;
}


// checking.. the song is playing or not
 function justplay(){
 	if(Playing_song==false){
 		playsong();

 	}else{
 		pausesong();
 	}
 }


// reset song slider
 function reset_slider(){
	 slider.value = 0;
 }

// play song
function playsong(){
  track.play();
  Playing_song = true;
  play.innerHTML = '<i class="fa fa-pause fa-2x white-text" aria-hidden="true"></i>';
}

//pause song
function pausesong(){
	track.pause();
	Playing_song = false;
	play.innerHTML = '<i class="fa fa-play fa-2x white-text" aria-hidden="true"></i>';
}


// next song
function next_song(){
	if(index_no < All_song.length - 1){
		index_no += 1;
		load_track(index_no);
		playsong();
	}else{
		index_no = 0;
		load_track(index_no);
		playsong();

	}
}


// previous song
function previous_song(){
	if(index_no > 0){
		index_no -= 1;
		load_track(index_no);
		playsong();

	}else{
		index_no = All_song.length;
		load_track(index_no);
		playsong();
	}
}


// change volume
function volume_change(){
	volume_show.innerHTML = recent_volume.value;
	track.volume = recent_volume.value / 100;
}

// change slider position 
function change_duration(){
	slider_position = track.duration * (slider.value / 100);
	track.currentTime = slider_position;
}

// autoplay function
function autoplay_switch() {
  if (autoplay == 1) {
    autoplay = 0;
  } else {
    autoplay = 1;
  }

  var element = document.getElementById("onauto");
  element.classList.toggle("activebtn");
}


function range_slider(){
	let position = 0;
        
        // update slider position
		if(!isNaN(track.duration)){
		   position = track.currentTime * (100 / track.duration);
		   slider.value =  position;
	      }

       
       // function will run when the song is over
       if(track.ended){
       	 play.innerHTML = '<i class="fa fa-play fa-2x white-text" aria-hidden="true"></i>';
           if(autoplay==1){
		       index_no += 1;
		       load_track(index_no);
		       playsong();
           }
	    }
     }