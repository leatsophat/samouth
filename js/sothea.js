let previous = document.querySelector('#pre');
let play = document.querySelector('#play');
let next = document.querySelector('#next');
let title = document.querySelector('#title');
let recent_volume= document.querySelector('#volume');
let volume_show = document.querySelector('#volume_show');
let slider = document.querySelector('#duration_slider');
let show_duration = document.querySelector('#show_duration');
let track_image = document.querySelector('#track_image');
let auto_play = document.querySelector('#auto');
let present = document.querySelector('#present');
let total = document.querySelector('#total');
let artist = document.querySelector('#artist');



let timer;
let autoplay = 0;

let index_no = 0;
let Playing_song = false;

//create a audio Element
let track = document.createElement('audio');



//All songs list
let All_song = [
	{
		name: "កញ្ញារូបស្រស់",
		path: "/asset/audios/sothea/កញ្ញារូបស្រស់-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "កណ្តូបបុកស្រូវ",
		path: "/asset/audios/sothea/កណ្តូបបុកស្រូវ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "កន្ទ្រែតមាសបង",
		path: "/asset/audios/sothea/កន្ទ្រែតមាសបង-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "កម្មស្នេហ៍ស្រីអាំ",
		path: "/asset/audios/sothea/កម្មស្នេហ៍ស្រីអាំ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "កម្លោះក្រមុំស្រុកស្រែ",
		path: "/asset/audios/sothea/កម្លោះក្រមុំស្រុកស្រែ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "កុំនឹកអូនអី",
		path: "/asset/audios/sothea/កុំនឹកអូនអី-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "កុំប្រកាន់បងអី",
		path: "/asset/audios/sothea/កុំប្រកាន់បងអី-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "កុំហៅឈ្មោះអូន",
		path: "/asset/audios/sothea/កុំហៅឈ្មោះអូន-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "កូនខឹងនឹងអ្នកណា",
		path: "/asset/audios/sothea/កូនខឹងនឹងអ្នកណា-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "កូលាបឥណ្ឌូនេស៊ី",
		path: "/asset/audios/sothea/កូលាបឥណ្ឌូនេស៊ី-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "កំលោះក្រមុំហ៊ឺហា",
		path: "/asset/audios/sothea/កំលោះក្រមុំហ៊ឺហា-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ក្បូនកាកី",
		path: "/asset/audios/sothea/ក្បូនកាកី-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ក្រមុំគិតច្រើន",
		path: "/asset/audios/sothea/ក្រមុំគិតច្រើន-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ក្រមុំព្រួលៗ (អុំទូកឆ្លងព្រែក)",
		path: "/asset/audios/sothea/ក្រមុំព្រួលៗ (អុំទូកឆ្លងព្រែក)-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ក្រមុំអារញ្ញ",
		path: "/asset/audios/sothea/ក្រមុំអារញ្ញ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ក្អមថ្មី",
		path: "/asset/audios/sothea/ក្អមថ្មី-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ខូចខ្លួនខូចប្រាណ",
		path: "/asset/audios/sothea/ខូចខ្លួនខូចប្រាណ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ខ្ញុំចង់រៀបការ",
		path: "/asset/audios/sothea/ខ្ញុំចង់រៀបការ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ខ្ញុំឈ្មោះជ័យជេត",
		path: "/asset/audios/sothea/ខ្ញុំឈ្មោះជ័យជេត-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ខ្មៅអើយខ្មៅ",
		path: "/asset/audios/sothea/ខ្មៅអើយខ្មៅ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "គង្គាទឹកភ្នែក",
		path: "/asset/audios/sothea/គង្គាទឹកភ្នែក-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "គូរព្រេងនិស្ស័យ",
		path: "/asset/audios/sothea/គូរព្រេងនិស្ស័យ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "គេថាខ្ញុំចាស់",
		path: "/asset/audios/sothea/គេថាខ្ញុំចាស់-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "គោចាស់ស្មៅខ្ចី",
		path: "/asset/audios/sothea/គោចាស់ស្មៅខ្ចី-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "គំនិតមួយពាន់បាប",
		path: "/asset/audios/sothea/គំនិតមួយពាន់បាប-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ឃើញស្នេហាខ្ញុំទេ",
		path: "/asset/audios/sothea/ឃើញស្នេហាខ្ញុំទេ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ចង់បានកន្សែងក្រហម",
		path: "/asset/audios/sothea/ចង់បានកន្សែងក្រហម-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ចង្រឹតស្រែកច្រៀង",
		path: "/asset/audios/sothea/ចង្រឹតស្រែកច្រៀង-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ចន្ទចាកមេឃ",
		path: "/asset/audios/sothea/ចន្ទចាកមេឃ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ចម្រៀងស្នេហា",
		path: "/asset/audios/sothea/ចម្រៀងស្នេហា-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ចម្រៀងស្នេហ៍ទុំទាវ",
		path: "/asset/audios/sothea/ចម្រៀងស្នេហ៍ទុំទាវ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ចាស់ហើយលោកតា",
		path: "/asset/audios/sothea/ចាស់ហើយលោកតា-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ចាស់ហើយលោកប្តី",
		path: "/asset/audios/sothea/ចាស់ហើយលោកប្តី-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ចាំមើលតែផ្លូវ",
		path: "/asset/audios/sothea/ចាំមើលតែផ្លូវ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ចាំ១០ខែទៀត",
		path: "/asset/audios/sothea/ចាំ១០ខែទៀត-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ចិត្តខ្លាចចិត្ត",
		path: "/asset/audios/sothea/ចិត្តខ្លាចចិត្ត-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ចិត្តតិចត្រីអាំង",
		path: "/asset/audios/sothea/ចិត្តតិចត្រីអាំង-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ចិត្តទេវទត្តមាត់ជាទេវតា",
		path: "/asset/audios/sothea/ចិត្តទេវទត្តមាត់ជាទេវតា-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ចិត្តឥតល្អៀង",
		path: "/asset/audios/sothea/ចិត្តឥតល្អៀង-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ចោរលួចចិត្ត",
		path: "/asset/audios/sothea/ចោរលួចចិត្ត-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ចៅដក",
		path: "/asset/audios/sothea/ចៅដក-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ឆាប់មកផ្ទះវិញ",
		path: "/asset/audios/sothea/ឆាប់មកផ្ទះវិញ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ឆ្នាំអូន16",
		path: "/asset/audios/sothea/ឆ្នាំអូន16-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ជួបភ័ក្រ្តជួបស្នេហ៍ (ម្ចាស់ខ្លុយស្នេហ៍)",
		path: "/asset/audios/sothea/ជួបភ័ក្រ្តជួបស្នេហ៍ (ម្ចាស់ខ្លុយស្នេហ៍)-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ជឿចិត្តបងចុះ",
		path: "/asset/audios/sothea/ជឿចិត្តបងចុះ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ជំនោរប៉ៃលិន",
		path: "/asset/audios/sothea/ជំនោរប៉ៃលិន-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "តូចអើយស្រីតូច",
		path: "/asset/audios/sothea/តូចអើយស្រីតូច-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "តើនៅឯណាស្នេហ៍ខ្ញុំ",
		path: "/asset/audios/sothea/តើនៅឯណាស្នេហ៍ខ្ញុំ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ថើបជួស",
		path: "/asset/audios/sothea/ថើបជួស-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ថ្ងៃព្រាត់យប់ជួប",
		path: "/asset/audios/sothea/ថ្ងៃព្រាត់យប់ជួប-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ថ្មគោលស្រមោលស្នេហ៍",
		path: "/asset/audios/sothea/ថ្មគោលស្រមោលស្នេហ៍-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ទឹកភ្លៀងលេងភ្លេង - អ៊ឺមសុងស៊ឺម រស់សេរីសុទ្ធា.mp3",
		path: "/asset/audios/sothea/ទឹកភ្លៀងលេងភ្លេង - អ៊ឺមសុងស៊ឺម រស់សេរីសុទ្ធា.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ទេសភាពឋានកណ្ដាល",
		path: "/asset/audios/sothea/ទេសភាពឋានកណ្ដាល-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ទៅលេងអូរជ្រៅ",
		path: "/asset/audios/sothea/ទៅលេងអូរជ្រៅ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ធាត់ឬស្គម",
		path: "/asset/audios/sothea/ធាត់ឬស្គម-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "នឹកគ្រប់ពេលវេលា",
		path: "/asset/audios/sothea/នឹកគ្រប់ពេលវេលា-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "នឹកណាស់",
		path: "/asset/audios/sothea/នឹកណាស់-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "នុះសង្សារខ្ញុំ",
		path: "/asset/audios/sothea/នុះសង្សារខ្ញុំ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "នៅតែស្រលាញ់",
		path: "/asset/audios/sothea/នៅតែស្រលាញ់-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "បងខឹងស្រីមួយណា",
		path: "/asset/audios/sothea/បងខឹងស្រីមួយណា-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "បងស្រលាញ់អូនប៉ុន្មានដែរ",
		path: "/asset/audios/sothea/បងស្រលាញ់អូនប៉ុន្មានដែរ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "បណ្តាំផ្តាំទៅសំឡូត",
		path: "/asset/audios/sothea/បណ្តាំផ្តាំទៅសំឡូត-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "បាត់ដំបងមានអ្វីឆ្ងាញ់",
		path: "/asset/audios/sothea/បាត់ដំបងមានអ្វីឆ្ងាញ់-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "បុប្ផាយន្ដហោះ",
		path: "/asset/audios/sothea/បុប្ផាយន្ដហោះ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "បែកក្អមអូនហើយ",
		path: "/asset/audios/sothea/បែកក្អមអូនហើយ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "បោចផ្កាម្រះព្រៅ",
		path: "/asset/audios/sothea/បោចផ្កាម្រះព្រៅ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "បំណុលកម្ម",
		path: "/asset/audios/sothea/បំណុលកម្ម-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "បំពេ",
		path: "/asset/audios/sothea/បំពេ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ប៉ៃលិនដួងចិត្ត",
		path: "/asset/audios/sothea/ប៉ៃលិនដួងចិត្ត-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ប៉ះដៃបានហើយ",
		path: "/asset/audios/sothea/ប៉ះដៃបានហើយ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ប្រយ័ត្នបាបណា",
		path: "/asset/audios/sothea/ប្រយ័ត្នបាបណា-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ប្រយ័ត្នឲ្យបងគេងក្រៅមុង",
		path: "/asset/audios/sothea/ប្រយ័ត្នឲ្យបងគេងក្រៅមុង-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ប្រស្នា4ខ",
		path: "/asset/audios/sothea/ប្រស្នា4ខ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ប្លែកណាស់",
		path: "/asset/audios/sothea/ប្លែកណាស់-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ផ្កាក្រវ៉ាន់",
		path: "/asset/audios/sothea/ផ្កាក្រវ៉ាន់-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ផ្ការីកផ្ការោយ",
		path: "/asset/audios/sothea/ផ្ការីកផ្ការោយ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ផ្កាអង្គារបុស្ស",
		path: "/asset/audios/sothea/ផ្កាអង្គារបុស្ស-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ផ្កាអើយផ្កាថ្កុល",
		path: "/asset/audios/sothea/ផ្កាអើយផ្កាថ្កុល-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ផ្គរលាន់ប៉ៃលិន",
		path: "/asset/audios/sothea/ផ្គរលាន់ប៉ៃលិន-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ផ្គរលាន់ឯត្បូង",
		path: "/asset/audios/sothea/ផ្គរលាន់ឯត្បូង-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ផ្សងជួបគ្រប់ជាតិ",
		path: "/asset/audios/sothea/ផ្សងជួបគ្រប់ជាតិ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ពេញចិត្តតែបងមួយ",
		path: "/asset/audios/sothea/ពេញចិត្តតែបងមួយ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ព្រមស្ម័គ្របងទៅ",
		path: "/asset/audios/sothea/ព្រមស្ម័គ្របងទៅ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ព្រលិតបឹងស្រី",
		path: "/asset/audios/sothea/ព្រលិតបឹងស្រី-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ព្រលឹងចុងសក់",
		path: "/asset/audios/sothea/ព្រលឹងចុងសក់-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ព្រោះនាងមានប្តី",
		path: "/asset/audios/sothea/ព្រោះនាងមានប្តី-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ភរិយាទាហាន",
		path: "/asset/audios/sothea/ភរិយាទាហាន-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ភូមិខ្មែរភូមិថ្មី",
		path: "/asset/audios/sothea/ភូមិខ្មែរភូមិថ្មី-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ភ្នែកខូច",
		path: "/asset/audios/sothea/ភ្នែកខូច-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ភ្នំតូចភ្នំធំ",
		path: "/asset/audios/sothea/ភ្នំតូចភ្នំធំ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "មាន់រងាវ",
		path: "/asset/audios/sothea/មាន់រងាវ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "មួយកំផ្លៀងអនុស្សាវរីយ៍ (ភ្លេងចាស់)",
		path: "/asset/audios/sothea/មួយកំផ្លៀងអនុស្សាវរីយ៍ (ភ្លេងចាស់)-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "មួយកំផ្លៀងអនុស្សាវរីយ៍ (ភ្លេងថ្មី)",
		path: "/asset/audios/sothea/មួយកំផ្លៀងអនុស្សាវរីយ៍ (ភ្លេងថ្មី)-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "មួយថ្ងៃល្អបីដង",
		path: "/asset/audios/sothea/មួយថ្ងៃល្អបីដង-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "មួយរាត្រីមួយជីវិត",
		path: "/asset/audios/sothea/មួយរាត្រីមួយជីវិត-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "មេម៉ាយថ្មីៗ",
		path: "/asset/audios/sothea/មេម៉ាយថ្មីៗ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ម្ចាស់ត្បូងកណ្តៀង",
		path: "/asset/audios/sothea/ម្ចាស់ត្បូងកណ្តៀង-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ម្តេចហ៊ានសួរខ្ញុំ",
		path: "/asset/audios/sothea/ម្តេចហ៊ានសួរខ្ញុំ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "យប់ផ្កាឈូក",
		path: "/asset/audios/sothea/យប់ផ្កាឈូក-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "យប់ពិសេស",
		path: "/asset/audios/sothea/យប់ពិសេស-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "យាមទៀតហើយឬបង",
		path: "/asset/audios/sothea/យាមទៀតហើយឬបង-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "រតនាវង្ស សុគន្ធារស",
		path: "/asset/audios/sothea/រតនាវង្ស សុគន្ធារស-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "រសស្នេហ៍ឧត្តម",
		path: "/asset/audios/sothea/រសស្នេហ៍ឧត្តម-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "រាងបងជូរអែម",
		path: "/asset/audios/sothea/រាងបងជូរអែម-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "រាត្រីត្រកាល",
		path: "/asset/audios/sothea/រាត្រីត្រកាល-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "រាំ Wolly Bully",
		path: "/asset/audios/sothea/រាំ Wolly Bully-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "រាំក្បាច់សារ៉ាវ៉ាន់",
		path: "/asset/audios/sothea/រាំក្បាច់សារ៉ាវ៉ាន់-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "រឿងស្នេហ៍ខ្ញុំ",
		path: "/asset/audios/sothea/រឿងស្នេហ៍ខ្ញុំ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "រំដួលភ្នំសំពៅ",
		path: "/asset/audios/sothea/រំដួលភ្នំសំពៅ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "លាបាត់ដំបង",
		path: "/asset/audios/sothea/លាបាត់ដំបង-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "លាហើយសូម៉ាត្រា",
		path: "/asset/audios/sothea/លាហើយសូម៉ាត្រា-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "លឺគេថាបងមក",
		path: "/asset/audios/sothea/លឺគេថាបងមក-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "លុះក្ស័យបងស្មោះ",
		path: "/asset/audios/sothea/លុះក្ស័យបងស្មោះ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "លួងលោមចៅស្រទបចេក",
		path: "/asset/audios/sothea/លួងលោមចៅស្រទបចេក-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ល្អណាស់ប្ដីខ្ញុំ",
		path: "/asset/audios/sothea/ល្អណាស់ប្ដីខ្ញុំ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "សត្វចាបយំចេប",
		path: "/asset/audios/sothea/សត្វចាបយំចេប-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "សន្យាបីថ្ងៃ",
		path: "/asset/audios/sothea/សន្យាបីថ្ងៃ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "សម្រស់នាងលក្ខស៊្មី",
		path: "/asset/audios/sothea/សម្រស់នាងលក្ខស៊្មី-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "សម្រស់អ្នកគ្រូក្រមុំ",
		path: "/asset/audios/sothea/សម្រស់អ្នកគ្រូក្រមុំ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "សម្លេងទាវឯក",
		path: "/asset/audios/sothea/សម្លេងទាវឯក-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "សាយ័ណ្ហព្រាត់ស្នេហ៍ (វាសនាបុប្ផាបាក់ព្រា)",
		path: "/asset/audios/sothea/សាយ័ណ្ហព្រាត់ស្នេហ៍ (វាសនាបុប្ផាបាក់ព្រា)-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "សារ៉ាវ៉ាន់ឆ្នាំថ្មី",
		path: "/asset/audios/sothea/សារ៉ាវ៉ាន់ឆ្នាំថ្មី-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "សុបិនចង្រៃ",
		path: "/asset/audios/sothea/សុបិនចង្រៃ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "សុបិន្តសួគ៌ា (ភ័ព្វព្រេងវាសនា)",
		path: "/asset/audios/sothea/សុបិន្តសួគ៌ា (ភ័ព្វព្រេងវាសនា)-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "សុរិយាព្រាត់គូ",
		path: "/asset/audios/sothea/សុរិយាព្រាត់គូ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "សុវណ្ណតែងអន",
		path: "/asset/audios/sothea/សុវណ្ណតែងអន-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "សុំខ្ចីគូរាំ",
		path: "/asset/audios/sothea/សុំខ្ចីគូរាំ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "សុំភ្លក់បានទេ",
		path: "/asset/audios/sothea/សុំភ្លក់បានទេ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "សោភ័ណ្ណមាសបង",
		path: "/asset/audios/sothea/សោភ័ណ្ណមាសបង-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្គាល់ខ្ញុំពីកាលណា",
		path: "/asset/audios/sothea/ស្គាល់ខ្ញុំពីកាលណា-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្គាល់រស់ជាតិស្នេហ៍",
		path: "/asset/audios/sothea/ស្គាល់រស់ជាតិស្នេហ៍-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្នេហាព្រះលក្សិនវង្ស",
		path: "/asset/audios/sothea/ស្នេហាព្រះលក្សិនវង្ស-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្នេហ៍ក្រមុំចាស់",
		path: "/asset/audios/sothea/ស្នេហ៍ក្រមុំចាស់-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្នេហ៍ក្រោមវាយោ",
		path: "/asset/audios/sothea/ស្នេហ៍ក្រោមវាយោ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្នេហ៍តែលុយ",
		path: "/asset/audios/sothea/ស្នេហ៍តែលុយ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្នេហ៍យើងនៅដដែល",
		path: "/asset/audios/sothea/ស្នេហ៍យើងនៅដដែល-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្នេហ៍លើពសុធា (ចិត្តបងសព្វថ្ងៃ)",
		path: "/asset/audios/sothea/ស្នេហ៍លើពសុធា (ចិត្តបងសព្វថ្ងៃ)-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្នេហ៍ស្រីអន",
		path: "/asset/audios/sothea/ស្នេហ៍ស្រីអន-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្នេហ៍អូនស្នេហ៍បង",
		path: "/asset/audios/sothea/ស្នេហ៍អូនស្នេហ៍បង-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្នេហ៍ឥតន័យ",
		path: "/asset/audios/sothea/ស្នេហ៍ឥតន័យ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្រលាញ់អូនដល់ណា",
		path: "/asset/audios/sothea/ស្រលាញ់អូនដល់ណា-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្រឡាញ់បងទៅ",
		path: "/asset/audios/sothea/ស្រឡាញ់បងទៅ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្រឡាញ់ផ្កាណា",
		path: "/asset/audios/sothea/ស្រឡាញ់ផ្កាណា-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្រអែមស្រីស្រស់",
		path: "/asset/audios/sothea/ស្រអែមស្រីស្រស់-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្រីតូច",
		path: "/asset/audios/sothea/ស្រីតូច-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្រីតូចច្រឡឹង",
		path: "/asset/audios/sothea/ស្រីតូចច្រឡឹង-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្រីស្រស់ប្រុសស្អាត",
		path: "/asset/audios/sothea/ស្រីស្រស់ប្រុសស្អាត-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្លឹកដូងស្លឹកចាក",
		path: "/asset/audios/sothea/ស្លឹកដូងស្លឹកចាក-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ស្អែក១ពាន់ដង",
		path: "/asset/audios/sothea/ស្អែក១ពាន់ដង-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ហ៊ានភ្នាល់ដាក់អី",
		path: "/asset/audios/sothea/ហ៊ានភ្នាល់ដាក់អី-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "អង្ករស្រូវថ្មី",
		path: "/asset/audios/sothea/អង្ករស្រូវថ្មី-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "អង្គុញកោយបី",
		path: "/asset/audios/sothea/អង្គុញកោយបី-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "អញ្ជើញ អញ្ជើញ",
		path: "/asset/audios/sothea/អញ្ជើញ អញ្ជើញ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "អណ្តាតមាស",
		path: "/asset/audios/sothea/អណ្តាតមាស-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "អណ្តូងទឹកខែវស្សា",
		path: "/asset/audios/sothea/អណ្តូងទឹកខែវស្សា-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "អនុភាពនៃពុកមាត់",
		path: "/asset/audios/sothea/អនុភាពនៃពុកមាត់-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "អនុស្សាវរីយ៌៣ខែត្រ",
		path: "/asset/audios/sothea/អនុស្សាវរីយ៌៣ខែត្រ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "អស់ផ្លូវហើយបង",
		path: "/asset/audios/sothea/អស់ផ្លូវហើយបង-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "អាឡូអូន អាឡូបង",
		path: "/asset/audios/sothea/អាឡូអូន អាឡូបង-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "អូនកើតឆ្នាំអី",
		path: "/asset/audios/sothea/អូនកើតឆ្នាំអី-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "អូនច្រៀងបំពេ",
		path: "/asset/audios/sothea/អូនច្រៀងបំពេ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "អូនរៀបការចុះ",
		path: "/asset/audios/sothea/អូនរៀបការចុះ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "អូរទឹកថ្លាបុប្ផាប៉ៃលិន",
		path: "/asset/audios/sothea/អូរទឹកថ្លាបុប្ផាប៉ៃលិន-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ឧត្តមភរិយាទាហាន",
		path: "/asset/audios/sothea/ឧត្តមភរិយាទាហាន-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ឫស្សីមួយដើម",
		path: "/asset/audios/sothea/ឫស្សីមួយដើម-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ឯណាព្រហ្មចារីយ៍",
		path: "/asset/audios/sothea/ឯណាព្រហ្មចារីយ៍-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
	  {
		name: "ឱ! ខ្លួនខ្ញុំអើយ",
		path: "/asset/audios/sothea/ឱ! ខ្លួនខ្ញុំអើយ-samouth.com.mp3",
		img: "/asset/images/sothea.jpg",
		singer: "រស់ សេរីសុទ្ធា"
	  },
  
];


// All functions


// function load the track
function load_track(index_no){
	clearInterval(timer);
	reset_slider();

	track.src = All_song[index_no].path;
	title.innerHTML = All_song[index_no].name;	
	track_image.src = All_song[index_no].img;
    artist.innerHTML = All_song[index_no].singer;
    track.load();

	timer = setInterval(range_slider ,1000);
	total.innerHTML = All_song.length;
	present.innerHTML = index_no + 1;
}

load_track(index_no);


//mute sound function
function mute_sound(){
	track.volume = 0;
	volume.value = 0;
	volume_show.innerHTML = 0;
}


// checking.. the song is playing or not
 function justplay(){
 	if(Playing_song==false){
 		playsong();

 	}else{
 		pausesong();
 	}
 }


// reset song slider
 function reset_slider(){
	 slider.value = 0;
 }

// play song
function playsong(){
  track.play();
  Playing_song = true;
  play.innerHTML = '<i class="fa fa-pause fa-2x white-text" aria-hidden="true"></i>';
}

//pause song
function pausesong(){
	track.pause();
	Playing_song = false;
	play.innerHTML = '<i class="fa fa-play fa-2x white-text" aria-hidden="true"></i>';
}


// next song
function next_song(){
	if(index_no < All_song.length - 1){
		index_no += 1;
		load_track(index_no);
		playsong();
	}else{
		index_no = 0;
		load_track(index_no);
		playsong();

	}
}


// previous song
function previous_song(){
	if(index_no > 0){
		index_no -= 1;
		load_track(index_no);
		playsong();

	}else{
		index_no = All_song.length;
		load_track(index_no);
		playsong();
	}
}


// change volume
function volume_change(){
	volume_show.innerHTML = recent_volume.value;
	track.volume = recent_volume.value / 100;
}

// change slider position 
function change_duration(){
	slider_position = track.duration * (slider.value / 100);
	track.currentTime = slider_position;
}

// autoplay function
function autoplay_switch() {
  if (autoplay == 1) {
    autoplay = 0;
  } else {
    autoplay = 1;
  }

  var element = document.getElementById("onauto");
  element.classList.toggle("activebtn");
}


function range_slider(){
	let position = 0;
        
        // update slider position
		if(!isNaN(track.duration)){
		   position = track.currentTime * (100 / track.duration);
		   slider.value =  position;
	      }

       
       // function will run when the song is over
       if(track.ended){
       	 play.innerHTML = '<i class="fa fa-play fa-2x white-text" aria-hidden="true"></i>';
           if(autoplay==1){
		       index_no += 1;
		       load_track(index_no);
		       playsong();
           }
	    }
     }